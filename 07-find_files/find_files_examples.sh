# Install locate for finding files (uncomment command if not installed)
#sudo apt install locate

# Update the locate database for finding files (uncomment command if not installed)
#sudo updatedb

# Find file names containing the word "df" in any part of the file on the file system
locate df

# Find file names matching the exact name "df" on the file system
locate -b '\df'

# Find a file name exactly matching "a.txt" from the current working directory
find -name a.txt

# Find a file name exactly matching "b.txt" from the current working directory
find -name b.txt

