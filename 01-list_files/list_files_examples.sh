# Simple ls example to list files in current directory.
# Output: ls_example.sh  test_folder  test_text_file1.txt  test_text_file2.txt
ls

# List all files including those usually hidden.
# Output: .  ..  ls_example.sh  test_folder  test_text_file1.txt  test_text_file2.txt
ls -a

# List the files unsorted.
# Output: test_folder  test_text_file2.txt  test_text_file1.txt  ls_example.sh
ls -U

# List the files with only one column (both commands do the same).
# Output:
#   ls_example.sh
#   test_folder
#   test_text_file1.txt
#   test_text_file2.txt
ls -1
ls -w1

# List files recursively.
# Output:
#  .:
#  ls_example.sh  test_folder  test_text_file1.txt  test_text_file2.txt
#
#  ./test_folder:
#  test_text_file3.txt  test_text_file4.txt
ls -R

