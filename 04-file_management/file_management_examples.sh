# NOTE: -v added for verbose output.

# Copy file test_text1.txt to test_text2.txt
cp -v ./test_text1.txt ./test_text2.txt

# Copy the contents of test_dir directory to new directory test_dir2
cp -v -r test_dir/ test_dir2/

# Use move command to rename a file from test_text2.txt to test_text3.txt
mv -v ./test_text2.txt ./test_text3.txt

# Move file test_text3.txt to directory test_dir
mv -v ./test_text3.txt test_dir/

# Delete the file test_dir/test_text3.txt
rm -v test_dir/test_text3.txt

# Delete the folder test_dir2 and all its contents
rm -v -r test_dir2/

# List the contents of the first few lines of a file (defaults to 10 lines)
head test_text1.txt

# List the contents of the first 3 lines of a file
head test_text1.txt -n3

# List the contents of the last few lines of a file (defaults to 10 lines)
tail test_text1.txt

# List the contents of the last 3 lines of a file
tail test_text1.txt -n3

