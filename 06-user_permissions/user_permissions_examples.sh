# List the current user (asuming bob is the output)
whoami

# List all users on your system
cat /etc/passwd

# Add administrator permissions to a file, assuming user is bob
sudo chown bob test_text1.txt

# Add administrator permissions recursively to all files within a directory
sudo chown bob -R test_dir

# Add a new user bob with password 1234
sudo useradd bob -p1234

# Delete a user
sudo userdel bob

