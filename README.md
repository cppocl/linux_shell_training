# Overview
Linux shell commands training to help learn the shell commands for basic file, directory and system management.  
All of the training will work with the Terminal application on a Linux Debian based distrobutions, including Ubuntu, Mint, Suse.  
  
NOTE: This training is not written for Fedora, Redhat, CentOS, and similar distributions, although the commands in most cases should be the same.  
  
If you are not sure of the distribution being used, run the following command in a Terminal to check:  
```
cat /etc/os-release
```
  
You can also see additional information with the following command:  
```
uname -a
```
  
You can also find you network name the following command:  
```
hostname
```

# Summary of Lessons

01 List files with command:  
```
ls
```
  
02 Disk information with commands:  
```
df
du
```
  
03 Directory management with commands:  
```
pwd
cd
mkdir
rmdir
```
  
04 File management with commands:  
```
cp
mv
rm
head
tail
```
  
05 File permissions with commands:  
```
whoami
who
chmod
```
  
06 User permissions with commands:  
```
chown
useradd
userdel
```
  
07 Find files with commands:  
```
locate
find
```
  
08 Processes with commands:  
```
ps
top
```
  
09 System information with command:  
```
dmesg
```
