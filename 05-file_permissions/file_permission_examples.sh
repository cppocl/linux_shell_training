# List current user
# Output: bob
whoami

# List current user with verbose information
# Output: bob      :1           2022-09-24 15:00 (:1)
who

# Add read and write permissions to the shell script
chmod +rw file_permission_examples.sh

# Add execute permissions to the shell script
chmod +x file_permission_examples.sh

