# List file system information
# Output:
#  udev            16399448        0  16399448   0% /dev
#  tmpfs            3290544    26596   3263948   1% /run
#  /dev/sdc2      926851424 11096500 868599776   2% /
#  /dev/sdc1         523248     3480    519768   1% /boot/efi
df

# List file system with file system type and in human readable form
#  udev           devtmpfs   16G     0   16G   0% /dev
#  tmpfs          tmpfs     3.2G   26M  3.2G   1% /run
#  /dev/sdc2      ext4      884G   11G  829G   2% /
#  /dev/sdc1      vfat      511M  3.4M  508M   1% /boot/efi
df -Th

# List the disk usage for each folder from the current working directory in human readable format
du -h

