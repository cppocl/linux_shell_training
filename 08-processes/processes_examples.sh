# Show running processes
ps

# Show all running processes including system processes
ps -A

# Show running shell commands
ps r

# Show running threads as if they were running processes
ps H

# Show a list of running processes with refresh
top

