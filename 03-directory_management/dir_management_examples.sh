# List current working directory
pwd

# Change directory to new working directory
cd test_dir

# Change directory to home directory
cd ~

# Change directory to previous working directory (back to test_dir)
cd -

#Change directory to parent directory
cd ..

# Change directory to a sub-directory of another directory
cd test_dir/test_dir2

# Change directory to two parent levels up
cd ../..

# Make a directory
mkdir temp

# Remove a directory
rmdir temp

